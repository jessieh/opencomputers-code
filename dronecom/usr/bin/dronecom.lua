local component = require("component")
local event = require("event")
local shell = require("shell")
local term = require("term")
local filesystem = require("filesystem")
local computer = require("computer")
local modem = nil
local gps = nil

-- Initialize the modem and GPS, if available.
if component.list("modem")() then modem = component.modem end
if component.list("navigation")() then gps = component.navigation end

-- -- -- ---------------- -- -- --
-- -- -- Check Connection -- -- --
-- -- -- ---------------- -- -- --

-- Define a function that checks our active connection.
local function getConnection()

   -- Check to see if we have an open connection.
   local port = 0
   if os.getenv("DRONE") and modem.isOpen(tonumber(os.getenv("DRONE"))) then
	  port = tonumber(os.getenv("DRONE"))
   else
	  return false
   end

   -- Check if the drone on this connection is still active.
   modem.broadcast(port, "return checkIn()")
   if select(6, event.pull(3, "modem_message")) ~= "STILL ALIVE" then
	  os.setenv("DRONE", nil)
	  modem.close(port)
	  return false
   else return port end

end

-- -- -- ------------- -- -- --
-- -- -- Connect Drone -- -- --
-- -- -- ------------- -- -- --

-- Define a function that connects to an unpaired drone.
local function connectDrone(specificPort)

   -- Check to see if the computer we're running on is up to snuff.
   if not modem or not modem.isWireless() then
	  print("The 'connect' function requires a wireless modem to run.")
	  os.exit(-2)
   end

   -- Check to see if we're already connected.
   do
	  local port = getConnection()
	  if port then
		 print("Already connected to a drone!")
		 print("Use 'dronecom disconnect' to disconnect from the paired drone.")
		 os.exit(-1)
	  end
   end

   -- Check to see if the port the user provided was valid.
   if specificPort and (tonumber(specificPort) < 1 or tonumber(specificPort) > 65535) then
	  print("Please enter a port number between 1 and 65535.")
	  os.exit(-3)
   end
   if specificPort and tonumber(specificPort) == 8675 then
	  print("You can not use port 8675 as a communications port.")
	  os.exit(-3)
   end

   -- Print some status text, and open up our port.
   print("Looking for an unpaired drone on port 8675...")
   modem.open(8675)

   -- Connect to the drone.
   do
	  local message = ""
	  local count = 1
	  while message ~= "CONNECTED" do
		 term.clearLine()
		 term.write("Attempt #" .. count)
		 modem.broadcast(8675, "connected = true")
		 message = select(6, event.pull(2, "modem_message"))
		 if count >= 5 then
			term.clearLine()
			print("Connection failed after " .. count .. " attempts.")
			os.exit(-1)
		 end
		 count = count + 1
	  end
	  term.clearLine()
	  print("Successful response: " .. message)
   end

   -- Print some more status text and initialize a variable to store our port.
   print("Handshake in progress...")
   local port = 0

   -- Handshake with the drone and agree on a new port.
   do
	  local newPort = 1
	  repeat
		 newPort = (tonumber(specificPort) or math.random(1, 65535))
	  until newPort ~= 8675
	  local message = ""
	  local count = 1
	  while message ~= newPort do
		 term.clearLine()
		 term.write("Attempt #" .. count)
		 modem.broadcast(8675, "newPort = " .. newPort)
		 message = select(6, event.pull(2, "modem_message"))
		 if count >= 5 then
			term.clearLine()
			print("Failed to agree on new port after " .. count .. " attempts.")
			os.exit(-1)
		 end
		 count = count + 1
	  end
	  term.clearLine()
	  print("Agreed on new port: " .. message)
	  port = newPort
   end

   -- Close the old port and open the new one, printing appropriate status text.
   print("Closing old port...")
   modem.close(8675)
   print("Opening connection on private port " .. port .. "...")
   modem.open(port)
   os.sleep(0.55)

   -- Reconnect to the drone on the new port.
   do
	  local message = ""
	  local count = 1
	  while message ~= "STILL ALIVE" do
		 term.clearLine()
		 term.write("Attempt #" .. count)
		 modem.broadcast(port, "connected = true")
		 message = select(6, event.pull(2, "modem_message"))
		 if count >= 5 then
			term.clearLine()
			print("Connection failed after " .. count .. " attempts.")
			os.exit(-1)
		 end
		 count = count + 1
	  end
	  term.clearLine()
	  print("Successful response: " .. message)
   end

   -- Set the drone's connection port as a global variable for future use.
   os.setenv("DRONE", port)
   print("Successfully connected to a drone on port: " .. os.getenv("DRONE"))

end

-- -- -- ------------ -- -- --
-- -- -- Print Status -- -- --
-- -- -- ------------ -- -- --

-- Define a function for printing the status of any open connection.
local function printStatus(quiet)

   -- If it's been specified that we remain silent, just return any open connection.
   if quiet then return getConnection() end

   -- List the address of any connected drone.
   local port = getConnection()
   if port then print("Current connection status: Open on port " .. port .. ".")
   else print("Current connection status: No open connection.") end

   -- List the version of the connected drone's BIOS.
   if port then
	  modem.broadcast(port, "return version()")
	  local message = select(6, event.pull(3, "modem_message"))
	  if message then print("BIOS version of connected drone: " .. message) end
   end

end

-- -- -- --------- -- -- --
-- -- -- SSH Drone -- -- --
-- -- -- --------- -- -- --

-- Define a function for sending commands and opening SSH sessions with a drone.
local function sshDrone(command)

   -- Check to see if we're already connected.
   local port = getConnection()
   if not port then
	  print("No currently connected drone!")
	  print("Use 'dronecom connect' to connect to an unpaired drone.")
	  os.exit(-1)
   end

   -- Begin giving commands to the drone in SSH mode if no command was already specified.
   if not command then
	  while true do

		 -- Prompt the user for a command.
		 print("Enter command or input 'exit' to stop:")
		 local command = io.read()

		 -- If no command was entered, scold the user. Otherwise, see if the user wants to exit.
		 if not command then
			print("Please enter a valid command.")
		 elseif command == "exit" then
			print("Exited successfully.")
			os.exit(1)
		 else

			-- If the user didn't leave us, broadcast the command to the drone and print out the returned message.
			modem.broadcast(port, command)
			local message = select(6, event.pull(5, "modem_message"))
			if message then print("Returned message: " .. tostring(message))
			else print("No returned message.") end

		 end

	  end
   else

	  -- Send the single command to the drone.
	  print("Sending command: " .. command)
	  modem.broadcast(port, command)

	  -- Print out the returned message.
	  local message = select(6, event.pull(5, "modem_message"))
	  if message then print("Returned message: " .. tostring(message))
	  else print("No returned message.") end

   end

end

-- -- -- ---------- -- -- --
-- -- -- Call Drone -- -- --
-- -- -- ---------- -- -- --

-- Define a function that calls the drone to this computer.
local function callDrone(hoverHeight)

   -- Check to see if the computer we're running on is up to snuff.
   if not gps or not gps.getPosition() then
	  print("The 'call' function requires a functioning navigation module to run.")
	  os.exit(-1)
   end

   -- Check to see if we're already connected.
   local port = getConnection()
   if not port then
	  print("No currently connected drone!")
	  print("Use 'dronecom connect' to connect to an unpaired drone.")
	  os.exit(-1)
   end

   -- Broadcast our location to the drone.
   local myX, myY, myZ = gps.getPosition()
   myY = myY + (hoverHeight or 1)
   print("Broadcasting current location to drone: " .. math.ceil(myX) .. ", " .. math.ceil(myY) .. ", " .. math.ceil(myZ))
   modem.broadcast(port, "flyTo(" .. myX .. ", " .. myY .. ", " .. myZ .. ")")

end

-- -- -- ------------ -- -- --
-- -- -- Follow Drone -- -- --
-- -- -- ------------ -- -- --

-- Define a function for having the drone follow this computer.
local function followDrone(followingHeight, pollingRate)

   -- Check to see if the computer we're running on is up to snuff.
   if not gps or not gps.getPosition() then
	  print("The 'follow' function requires a functioning navigation module to run.")
	  os.exit(-1)
   end

   -- Check to see if we're already connected.
   local port = getConnection()
   if not port then
	  print("No currently connected drone!")
	  print("Use 'dronecom connect' to connect to an unpaired drone.")
	  os.exit(-1)
   end

   -- Check to see if the drone has a leash available.
   modem.broadcast(port, "return isComponentAvailable('leash')")
   local leashAvailable = select(6, event.pull(3, "modem_message"))

   -- Let the player know how to use the program, and then change the drone's light color.
   print("Press 'Q' to exit.")
   if leashAvailable then
	  print("Press 'L' to leash any nearby mobs.")
	  print("Press 'U' to unleash any leashed mobs.")
   end
   print("Broadcasting location...")
   modem.broadcast(port, "drone.setLightColor(lightColor.white)")

   -- Define a variable to store the ID of any pressed keys in and start broadcasting our location.
   local keypressed = 0
   while keypressed ~= 16 do

	  -- Get the most recent keypress and grab our location from the GPS.
	  keypressed = select(4, event.pull(pollingRate or 1, "key_down"))
	  local myX, myY, myZ = gps.getPosition()

	  -- If 'L' was pressed, fly down towards the ground and try to leash any nearby mobs.
	  if leashAvailable and keypressed == 38 then

		 -- Calculate a lower following height (for leashing) and broadcast our location to the drone.
		 myY = myY - 1.3
		 term.clearLine()
		 term.write("Leashing nearest mobs: " .. math.ceil(myX) .. ", " .. math.ceil(myY) .. ", " .. math.ceil(myZ))
		 do
			modem.broadcast(port, "flyTo(" .. myX .. ", " .. myY .. ", " .. myZ .. ")")
			event.pull(5, "modem_message")
		 end

		 -- Send a command to the drone instructing it to leash anything nearby.
		 do
			modem.broadcast(port, "leashNearest()")
			event.pull(5, "modem_message")
		 end

	  else

		 -- Calculate our following height and broadcast our location to the drone.
		 myY = myY + (followingHeight or 1)
		 term.clearLine()
		 term.write("Broadcasting location to drone: " .. math.ceil(myX) .. ", " .. math.ceil(myY) .. ", " .. math.ceil(myZ))
		 do
			modem.broadcast(port, "flyTo(" .. myX .. ", " .. myY .. ", " .. myZ .. ")")
			event.pull(5, "modem_message")
		 end

		 -- If the 'U' key was pressed, unleash any leashed mobs.
		 if leashAvailable and keypressed == 22 then
			term.clearLine()
			term.write("Unleashing leashed mobs: " .. math.ceil(myX) .. ", " .. math.ceil(myY) .. ", " .. math.ceil(myZ))
			modem.broadcast(port, "leash.unleash()")
			event.pull(5, "modem_message")
		 end

	  end

   end

   -- Change the drone's light color back.
   term.clearLine()
   print("Following routine stopped.")
   modem.broadcast(port, "drone.setLightColor(lightColor.green)")

end

-- -- -- ------------- -- -- --
-- -- -- Program Drone -- -- --
-- -- -- ------------- -- -- --

-- Define a function for downloading a program to a drone and running it.
local function programDrone(filePath)

   -- Check to see if we're already connected.
   local port = getConnection()
   if not port then
	  print("No currently connected drone!")
	  print("Use 'dronecom connect' to connect to an unpaired drone.")
	  os.exit(-1)
   end

   -- Make sure the user specified a file.
   if not filePath then
	  print("No file specified.")
	  print("Use 'dronecom program <file>' to upload programs to the drone.")
	  os.exit(-1)
   end

   -- Find the specified file.
   print("Opening specified file...")
   local file, error = io.open(filePath, "r")
   if not file then
	  print("Failed to open file: " .. error)
	  os.exit(-1)
   end

   -- Read the file.
   print("Reading file...")
   local data = file:read("*all")
   file:close()

   -- Upload the program to the drone.
   print("Uploading program to drone: " .. filePath)
   modem.broadcast(port, data)
   local message = select(6, event.pull(5, "modem_message"))

   -- Print the returned message, if any.
   if message then print("Returned message: " .. tostring(message))
   else print("No returned message.") end

end

-- -- -- ---------------- -- -- --
-- -- -- Disconnect Drone -- -- --
-- -- -- ---------------- -- -- --

-- Define a function for disconnecting from a drone.
local function disconnectDrone(shutdown)

   -- Check to see if we're already connected.
   local port = getConnection()
   if not port then
	  print("No currently connected drone!")
	  print("Use 'dronecom connect' to connect to an unpaired drone.")
	  os.exit(-1)
   end

   -- Shutdown or reset the drone, and disconnect.
   if shutdown then
	  print("Shutting down drone...")
	  modem.broadcast(port, "shutdown()")
   else
	  print("Rebooting drone...")
	  modem.broadcast(port, "reboot()")
   end
   print("Closing port...")
   modem.close(port)
   os.setenv("DRONE", nil)
   print("Disconnected successfully.")

end

-- -- -- ----------- -- -- --
-- -- -- Flash Drone -- -- --
-- -- -- ----------- -- -- --

-- Define a function that writes the drone BIOS to an EEPROM chip.
local function flashDrone(filePath)

   -- Check to see if we're running on a tablet. If we are, abandon ship.
   if component.list("tablet")() then
	  print("Flashing EEPROM chips directly cannot be done on tablets.")
	  print("Please update the EEPROM wirelessly or use a desktop computer.")
	  os.exit(-1)
   end

   -- Ask the user if they're ready to flash. Cancel if they aren't.
   print("Please insert the desired EEPROM chip to flash.")
   print("Ready to flash? [Y/N]")
   local response = ""
   while response:lower():sub(1, 1) ~= "y" do
	  response = io.read()
	  if not response then response = ""
	  elseif response:lower():sub(1, 1) == "n" then
		 print("Flash operation has been cancelled.")
		 os.exit(-1)
	  elseif response:lower():sub(1, 1) ~= "y" then print("Invalid input. Please try again:") end
   end

   -- Tell the user that the process is starting and define a variable for the EEPROM chip.
   print("Beginning flash process.")

   -- Find the specified file. If one isn't specified, go for the default.
   if filePath then print("Opening specified BIOS file...")
   else print("Opening BIOS file...") end
   local filepath = filePath or "/usr/share/dronecom/bios/dronecombios.lua"
   local file, error = io.open(filepath, "r")
   if not file then
	  print("Failed opening file: " .. error)
	  os.exit(-1)
   end

   -- Read the file.
   print("Reading file...")
   local data = file:read("*a")
   file:close()

   -- Print the EEPROM's address and a warning for the user while the flashing starts.
   local eeprom = component.eeprom
   print("Flashing EEPROM: " .. eeprom.address .. "...")
   print("Please do NOT power down or restart your computer during this process!")

   -- Flash the EEPROM and set its new label. Then, let the user know that we're done.
   eeprom.set(data)
   eeprom.setLabel("dronecom")
   print("Flashing complete! The EEPROM chip is now safe to remove.")

end

-- -- -- ------------ -- -- --
-- -- -- Update Drone -- -- --
-- -- -- ------------ -- -- --

-- Define a function that updates the drone's EEPROM chip wirelessly.
local function updateDrone(filePath)

   -- Check to see if we're already connected.
   local port = getConnection()
   if not port then
	  print("No currently connected drone!")
	  print("Use 'dronecom connect' to connect to an unpaired drone.")
	  os.exit(-1)
   end

   -- Ask the user if they're ready to flash. Cancel if they aren't.
   print("Please make sure that your drone is ready to be flashed.")
   print("Ready to flash wirelessly? [Y/N]")
   local response = ""
   while response:lower():sub(1, 1) ~= "y" do
	  response = io.read()
	  if not response then response = ""
	  elseif response:lower():sub(1, 1) == "n" then
		 print("Flash operation has been cancelled.")
		 os.exit(-1)
	  elseif response:lower():sub(1, 1) ~= "y" then print("Invalid input. Please try again:") end
   end

   -- Tell the user that the process is starting and have the drone define a variable for the EEPROM chip.
   print("Beginning flash process.")
   modem.broadcast(port, "eeprom = component.proxy(component.list('eeprom')())")

   -- Find the specified file. If one isn't specified, go for the default.
   if filePath then print("Opening specified BIOS file...")
   else print("Opening BIOS file...") end
   local filepath = filePath or "/usr/share/dronecom/bios/dronecombios.lua"
   local file, error = io.open(filepath, "r")
   if not file then
	  print("Failed opening file: " .. error)
	  os.exit(-1)
   end

   -- Read the file.
   print("Reading file...")
   local data = file:read("*a")
   file:close()

   -- Print the drone's port and a warning for the user while the flashing starts.
   print("Flashing EEPROM of drone on port: " .. port .. ".")
   print("Please do NOT power down the computer or drone during this process!")

   -- Flash the EEPROM and set its new label.
   do
	  modem.broadcast(port, "eeprom.set([[" .. data .. "]])")
	  event.pull(15, "modem_message")
   end
   do
	  modem.broadcast(port, "eeprom.setLabel('dronecom')")
	  event.pull(5, "modem_message")
   end

   -- Disconnect from the drone, reboot it, and reconnect to it.
   print("Flashing complete!")
   disconnectDrone()
   connectDrone(port)

   -- All done!
   print("Update successfully flashed.")

end

-- -- -- ------------- -- -- --
-- -- -- Print Version -- -- --
-- -- -- ------------- -- -- --

-- Define a function that prints the current version of dronecom.
local function printVersion(quiet)

   -- Define the current version of dronecom.
   local version = "v1.95"

   -- If it's been specified that we remain silent, just return the program version early.
   if quiet then return version end

   -- Print the current version of dronecom and any other relevant information.
   print("dronecom " .. version .. " - Drone communications software for OpenOS.")
   print("Programmed by Jessie Hildebrandt.")
   return version

end

-- -- -- --------- -- -- --
-- -- -- Make Disk -- -- --
-- -- -- --------- -- -- --

-- Define a function that creates dronecom installation disks.
local function makeDisk()

   -- Define a function that checks if a folder exists and creates it if it doesn't.
   local function createFolder(folderPath, diskPath)
	  local folderPath = diskPath .. folderPath
	  if not filesystem.exists(folderPath) then
		 filesystem.makeDirectory(folderPath)
		 print("Created directory: " .. folderPath)
	  end
   end

   -- Define a function that verbosely copies a file from the disk to the system.
   local function copyFile(filePath, diskPath)
	  local destination = diskPath .. filePath
	  filesystem.copy(filePath, destination)
	  print("Copied file: " .. destination)
   end

   -- Generate a list of writable disks to install to.
   local devices = {}
   for address in component.list("filesystem") do
	  local dev = component.proxy(address)
	  if not dev.isReadOnly() and dev.address ~= computer.tmpAddress() then
		 table.insert(devices, dev)
	  end
   end

   -- If there are no writable disks, abort the installation.
   if #devices == 0 then
	  print("No writable disks found, aborting.")
	  os.exit(-1)
   end

   -- Print the available choices for the user to choose from.
   print("This will reformat a device as a dronecom installation disk.")
   print("To select a device to reformat, enter a number between 1 and " .. #devices .. ".")
   print("Or, enter 'q' to abort the wizard.")
   for i = 1, #devices do
	  local label = devices[i].getLabel()
	  if label then label = label .. " (" .. devices[i].address:sub(1, 8) .. "...)"
	  else label = devices[i].address end
	  print("[" .. i .. "] " .. label)
   end
   print("Please input a number:")

   -- Accept input from the user.
   local chosenDevice = nil
   while not chosenDevice do
	  local response = io.read()
	  if response then
		 if response:sub(1, 1):lower() == "q" then
			print("Disk creation process has been cancelled.")
			os.exit(-1)
		 end
		 local number = tonumber(response)
		 if number and number > 0 and number <= #devices then chosenDevice = devices[number]
		 else print("Invalid input, please try again:") end
	  end
   end

   -- Set up the installation path.
   local diskPath = "/mnt/" .. chosenDevice.address:sub(1,3)

   -- Clear the disk before installing.
   print("Clearing device...")
   for file in filesystem.list(diskPath .. "/") do
	  filesystem.remove(file)
	  print("Removed: " .. file)
   end

   -- Set up the directory structure.
   print("Creating directories...")
   createFolder("/usr/bin/dronecom/", diskPath)
   createFolder("/usr/share/dronecom/bios", diskPath)

   -- Copy over the files.
   print("Copying files...")
   copyFile("/usr/bin/dronecom.lua", diskPath)
   copyFile("/usr/share/dronecom/bios/dronecombios.lua", diskPath)

   -- Set the label of the disk.
   print("Applying label...")
   local disk = filesystem.get(diskPath .. "/")
   disk.setLabel("dronecom")

   -- Let the user know that we're finished.
   print("Finished creating installation disk. You may now eject the device.")

end

-- -- -- ------------ -- -- --
-- -- -- Command Info -- -- --
-- -- -- ------------ -- -- --

-- Define a function that prints information on the specified command.
local function commandInfo(command)

   -- Define a table of help articles for each command.
   local helpArticle = {
	  ["connect"] = {
		 "'dronecom connect [PORT]' - Connects to and pairs with an unpaired drone.",
		 " PORT: If specified, pairs with the unpaired drone on PORT."
	  },
	  ["status"] = {
		 "'dronecom status [-q]' - Outputs the current status of the drone connection.",
		 " -q: If specified, outputs nothing but returns current port number."
	  },
	  ["ssh"] = {
		 "'dronecom ssh [COMMAND]' - Opens a ssh session with the connected drone.",
		 " COMMAND: If specified, send COMMAND to the drone and output result."
	  },
	  ["call"] = {
		 "'dronecom call [HEIGHT]' - Calls a connected drone over to this machine.",
		 " HEIGHT: If specified, the drone will hover HEIGHT meters over the machine.",
		 "           Defaults to 1 if unspecified."
	  },
	  ["follow"] = {
		 "'dronecom follow [HEIGHT] [RATE]' - Has the connected drone follow this machine.",
		 " HEIGHT: If specified, the drone will hover HEIGHT meters over the computer.",
		 "           Defaults to 1 if unspecified.",
		 " RATE: If specified, the drone will fly to the machine every RATE seconds.",
		 "         Defaults to 1 if unspecified."
	  },
	  ["program"] = {
		 "'dronecom program FILE' - Uploads FILE to the connected drone for execution.",
		 " FILE: The path of the file to upload to the drone."
	  },
	  ["disconnect"] = {
		 "'dronecom disconnect [-s]' - Disconnect from the connected drone and reset it.",
		 " -s: If specified, shuts down the connected drone instead of resetting it."
	  },
	  ["flash-eeprom"] = {
		 "'dronecom flash-eeprom [FILE]' - Flashes the current EEPROM with a dronecom BIOS.",
		 " FILE: If specified, flashes the file at the provided path.",
		 "         Defaults to '/usr/share/dronecom/bios/dronecombios.lua' if unspecified."
	  },
	  ["update-drone"] = {
		 "'dronecom update-drone [FILE]' - Wirelessly updates the EEPROM of a drone.",
		 " FILE: If specified, flashes the file at the provided path.",
		 "         Defaults to '/usr/share/dronecom/bios/dronecombios.lua' if unspecified."
	  },
	  ["version"] = {
		 "'dronecom version [-q]' - Prints the current version of dronecom.",
		 " -q: If specified, outputs nothing but returns the version number as a string."
	  },
	  ["makedisk"] = {
		 "'dronecom makedisk' - Opens a utility that creates dronecom installation disks."
	  },
	  ["help"] = {
		 "'dronecom help COMMAND' - Displays information about a dronecom command.",
		 " COMMAND: The command to display information about."
	  },
	  ["uninstall"] = {
		 "'dronecom uninstall' - Opens a utility that uninstalls dronecom from the system"
	  },
	  ["unspecified"] = {
		 "Invalid command specified.",
		 "Use 'dronecom help COMMAND' to get information about a command."
	  }
   }

   -- Print the appropriate help information from our table.
   if not helpArticle[command] then command = "unspecified" end
   for line = 1, #helpArticle[command] do
	  print(helpArticle[command][line])
   end

end

-- -- -- ----------------- -- -- --
-- -- -- Uninstall Program -- -- --
-- -- -- ----------------- -- -- --

-- Define a function that uninstalls the program from the system.
local function uninstallProgram()

   -- Define a function that checks if a folder exists and verbosely deletes it if it does.
   local function deleteFolder(folderPath)
	  local folderPath = folderPath
	  if filesystem.exists(folderPath) then
		 filesystem.remove(folderPath)
		 print("Deleted directory: " .. folderPath)
	  end
   end

   -- Define a function that verbosely deletes a file from the system.
   local function deleteFile(filePath)
	  if filesystem.exists(filePath) then
		 filesystem.remove(filePath)
		 print("Deleted file: " .. filePath)
	  end
   end

   -- Ask the user if they'd really like to uninstall dronecom.
   print("Are you sure you'd like to uninstall dronecom? [Y/N]")
   local response = ""
   while response:lower():sub(1, 1) ~= "y" do
	  response = io.read()
	  if not response then response = ""
	  elseif response:lower():sub(1, 1) == "n" then
		 print("Uninstall process has been cancelled.")
		 os.exit(-1)
	  elseif response ~= "y" then print("Invalid input. Please try again:") end
   end

   -- Remove the files.
   print("Removing files...")
   deleteFile("/usr/bin/dronecom.lua")
   deleteFile("/usr/share/dronecom/bios/dronecombios.lua")

   -- Remove the directories.
   print("Removing directories...")
   deleteFolder("/usr/share/dronecom/bios/")
   deleteFolder("/usr/share/dronecom/")

   -- Let the user know that dronecom has been uninstalled successfully.
   print("Uninstall process complete! Thank you for using dronecom.")

end

-- -- -- ----------- -- -- --
-- -- -- Print Usage -- -- --
-- -- -- ----------- -- -- --

-- Define a function that prints proper program usage instructions.
local function printUsage()
   print("Usage: dronecom <command> [options]")
   print("'dronecom connect' to connect to an unpaired drone.")
   print("'dronecom ssh' to open a ssh session with the drone.")
   print("'dronecom program FILE' to upload FILE to the drone and run it.")
   print("'dronecom disconnect' to disconnect from a paired drone.")
   print("'dronecom help COMMAND' to learn more about any given command.")
   print("'man dronecom' for a full list of commands and instructions.")
end

-- -- -- --------------- -- -- --
-- -- -- Parse Arguments -- -- --
-- -- -- --------------- -- -- --

-- Parse any arguments provided to us by the user.
local args, options = shell.parse(...)

-- Process our provided arguments and act as needed.
if args[1] == "connect" then
   return connectDrone(args[2])

elseif args[1] == "status" then
   return printStatus(options.q)

elseif args[1] == "ssh" then
   return sshDrone(args[2])

elseif args[1] == "call" then
   return callDrone(args[2])

elseif args[1] == "follow" then
   return followDrone(args[2], args[3])

elseif args[1] == "program" then
   return programDrone(args[2])

elseif args[1] == "disconnect" then
   return disconnectDrone(options.s)

elseif args[1] == "flash-eeprom" then
   return flashDrone(args[2])

elseif args[1] == "update-drone" then
   return updateDrone(args[2])

elseif args[1] == "version" then
   return printVersion(options.q)

elseif args[1] == "makedisk" then
   return makeDisk()

elseif args[1] == "help" then
   return commandInfo(args[2])

elseif args[1] == "uninstall" then
   return uninstallProgram()

else printUsage() end
