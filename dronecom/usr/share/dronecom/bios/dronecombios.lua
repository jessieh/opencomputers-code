drone = component.proxy(component.list("drone")())
modem = nil
gps = nil
leash = nil

if component.list("modem")() then modem = component.proxy(component.list("modem")()) end
if component.list("navigation")() then gps = component.proxy(component.list("navigation")()) end
if component.list("leash")() then leash = component.proxy(component.list("leash")()) end

local port = 8675

lightColor = {
   red = 16711680,
   yellow = 16776960,
   green = 65280,
   cyan = 65535,
   blue = 255,
   purple = 8388863,
   pink = 16711935,
   white = 16777215
}

drone.setLightColor(lightColor.yellow)

local function respond(...)
   local args = table.pack(...)
   pcall(function() modem.broadcast(port, table.unpack(args)) end)
end

local function receive(timeout)
   while true do
	  local event, _, _, _, _, command = computer.pullSignal(timeout)
	  if event == "modem_message" and command then return load(command) end
	  errorMessage("E: TIMEOUT")
   end
end

local function errorMessage(errorMessage)
   drone.setStatusText(errorMessage)
   drone.setLightColor(lightColor.red)
   computer.beep(988, 0.6)
   os.sleep(5)
   return computer.shutdown()
end

function version()
   return "v1.93"
end

function shutdown()
   drone.setLightColor(lightColor.yellow)
   computer.beep(1046)
   computer.beep(523)
   drone.setStatusText("OFF       ")
   return computer.shutdown()
end

function reboot()
   drone.setLightColor(lightColor.yellow)
   drone.setStatusText("REBOOTING ")
   return computer.shutdown(true)
end

function checkIn()
   return "STILL ALIVE"
end

function move(x, y, z)
   return drone.move(x, y, z)
end

function flyTo(x, y, z)
   local myX, myY, myZ = gps.getPosition()
   return drone.move(x - myX, y - myY, z - myZ)
end

function isComponentAvailable(component)
   return component.list(component)() ~= nil
end

function leashNearest()
   local leashed = false
   for side = 1, 5 do leash = leash.leash(side) end
   return leashed
end

function resetModem()

   connected = nil
   newPort = nil
   port = 8675
   modem.open(8675)

   drone.setStatusText("SEARCHING ")
   drone.setLightColor(lightColor.pink)
   computer.beep()

   while not connected do
	  local message, reason = pcall(function()
			local message = receive()
			if not message then errorMessage("E: SEARCH ") end
			message()
	  end)
	  if not message then errorMessage("E: SEARCH ") end
   end

   modem.broadcast(8675, "CONNECTED")
   drone.setStatusText("HANDSHAKE ")
   drone.setLightColor(lightColor.blue)

   do
	  while not newPort do
		 local message, error = pcall(function()
			   local message, error = receive(10)
			   if not message then errorMessage("E: HNDSHK.") end
			   message()
		 end)
		 if not message then errorMessage("E: HNDSHK.") end
	  end
	  port = newPort
   end

   modem.broadcast(8675, port)
   drone.setStatusText("RECONNECT ")
   drone.setLightColor(lightColor.cyan)

   connected = nil
   modem.close(8675)
   modem.open(port)

   while not connected do
	  local message, error = pcall(function()
			local message, error = receive(10)
			if not message then errorMessage("E: RCNNCT.") end
			message()
	  end)
	  if not message then errorMessage("E: RCNNCT.") end
   end

   modem.broadcast(port, "STILL ALIVE")
   drone.setStatusText("CONNECTED ")
   drone.setLightColor(lightColor.green)
   computer.beep(523)
   computer.beep(1046)

end

if gps then
   if not gps.getPosition() then errorMessage("E: BAD GPS") end
else errorMessage("E: NO GPS ") end

if not modem then
   errorMessage("E: NOMODEM")
end

if not modem.isWireless() then
   errorMessage("E: !WMODEM")
end

resetModem()

while true do
   local message, error = pcall(function()
		 local message, error = receive()
		 if not message then return respond(error) end
		 respond(message())
   end)
   if not message then respond(error) end
end
