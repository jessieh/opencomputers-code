# dronecom

Drone communications and control utilities for the computers and drones of the OpenComputers mod.

### TODO:

* Fix drone disconnect on update error
* Fix floppy disk detecting itself during install
* Fix leashing functionality in drone bios
* Fix component list in bios
