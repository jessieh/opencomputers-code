----------------------------------------
-- componentMgr
-- Component Manager Module for basepan
-- Programmed by Jessie Hildebrandt
----------------------------------------

----------------------------------------
-- Module requirements
----------------------------------------

local component = require( 'component' )

----------------------------------------
-- Module prototype
----------------------------------------

local componentMgr = {}

----------------------------------------
-- Constants
----------------------------------------

local COLOR = {
   white = 0,
   orange = 1,
   darkPink = 2,
   blue = 3,
   yellow = 4,
   green = 5,
   pink = 6,
   grey = 7,
   lightGrey = 8,
   darkBlue = 9,
   purple = 10,
   indigo = 11,
   brown = 12,
   olive = 13,
   red = 14,
   black = 15
}

----------------------------------------
-- Local variables
----------------------------------------

local registeredComponents = {}

local commonBehaviors = {}
local defaultBehaviors = {}
local componentBehaviors = setmetatable( {}, { __index = function( table, key )
												  return defaultBehaviors[ key ]
									   end } )

local readableNames = setmetatable( {}, { __index = function( table, key )
											 return "Unknown Component"
								  end } )

----------------------------------------
-- Common component behaviors
----------------------------------------

-- Common Component Behaviors
commonBehaviors = {

   -- Generic IC2 Energy Storage
   ["ic2_energy"] = {
	  tag = nil,
	  description = nil,
	  blockType = "color",
	  variables = {},
	  sharedVariables = {},
	  graphs = {
		 {
            colors = {
               {
                  fgColor = COLOR.grey,
                  bgColor = COLOR.red
               },
               {
                  fgColor = COLOR.grey,
                  bgColor = COLOR.orange
               },
               {
                  fgColor = COLOR.grey,
                  bgColor = COLOR.yellow
               },
               {
                  fgColor = COLOR.grey,
                  bgColor = COLOR.green
               }
            },
			getCurrentVal = function( object )
			   return object.getEnergy()
			end,
			getMaxVal = function( object )
			   return object.getCapacity()
			end,
		 },
	  },
	  getCurrentValStr = function( object )
		 return "CUR " .. math.floor( object.getEnergy() )
	  end,
	  getMaxValStr = function( object )
		 return "CAP " .. math.floor( object.getCapacity() )
	  end,
	  onActivate = nil,
   }

}

----------------------------------------
-- Default component behaviors
----------------------------------------

-- Default Component Behaviors
defaultBehaviors = {

   -- Advanced Gen. Steam Turbine
   ["ag_steam_turbine"] = {
	  tag = nil,
	  description = nil,
	  blockType = "histogram",
	  variables = {},
	  sharedVariables = {},
	  graphs = {
		 {
			fgColor = COLOR.green,
			bgColor = COLOR.grey,
			getCurrentVal = function( object )
			   return object.getEnergyStored()
			end,
			getMaxVal = function( object )
			   return object.getEnergyCapacity()
			end
		 },
		 {
			fgColor = COLOR.orange,
			bgColor = COLOR.grey,
			getCurrentVal = function( object )
			   return object.getInfo["production_current"]
			end,
			getMaxVal = nil
		 }
	  },
	  getCurrentValStr = function( object )
		 if ( object.getControls()[ "useSteam" ] == "ENABLED" ) then
			return "CUR " .. object.getEnergyStored()
		 else
			return ""
		 end
	  end,
	  getMaxValStr = function( object )
		 if ( object.getControls()[ "useSteam" ] == "ENABLED" ) then
			return "CAP " .. object.getEnergyCapacity()
		 else
			return "-DISABLED-"
		 end
	  end,
	  onActivate = function( object )
		 if ( object.getControls()[ "useSteam" ] == "ENABLED" ) then
			object.setControlStatus( "useSteam", "DISABLED" )
		 else
			object.setControlStatus( "useSteam", "ENABLED" )
		 end
	  end
   },

   -- Advanced Gen. Heat Exchanger
   ["ag_heat_exchanger"] = {
	  tag = nil,
	  description = nil,
	  blockType = "histogram",
	  variables = {},
	  sharedVariables = {},
	  graphs = {
		 {
			fgColor = COLOR.white,
			bgColor = COLOR.grey,
			getCurrentVal = function( object )
			   return object.getInfo()[ "output_hot" ][ "amount" ]
			end,
			getMaxVal = function( object )
			   return object.getInfo()[ "output_hot" ][ "capacity" ]
			end
		 },
		 {
			fgColor = COLOR.red,
			bgColor = COLOR.grey,
			getCurrentVal = function( object )
			   return object.getInfo()[ "input_hot" ][ "amount" ]
			end,
			getMaxVal = function( object )
			   return object.getInfo()[ "input_hot" ][ "capacity" ]
			end
		 },
		 {
			fgColor = COLOR.darkBlue,
			bgColor = COLOR.grey,
			getCurrentVal = function( object )
			   return object.getInfo()[ "input_cold" ][ "amount" ]
			end,
			getMaxVal = function( object )
			   return object.getInfo()[ "input_cold" ][ "capacity" ]
			end
		 }
	  },
	  getCurrentValStr = function( object )
		 if ( object.getControls()[ "exchangeHeat" ] == "ENABLED" ) then
			return "CUR " .. object.getInfo()[ "output_hot" ][ "amount" ]
		 else
			return ""
		 end
	  end,
	  getMaxValStr = function( object )
		 if ( object.getControls()[ "exchangeHeat" ] == "ENABLED" ) then
			return "CAP " .. object.getInfo()[ "output_hot" ][ "capacity" ]
		 else
			return "-DISABLED-"
		 end
	  end,
	  onActivate = function( object )
		 if ( object.getControls()[ "exchangeHeat" ] == "ENABLED" ) then
			object.setControlStatus( "exchangeHeat", "DISABLED" )
		 else
			object.setControlStatus( "exchangeHeat", "ENABLED" )
		 end
	  end
   },

   -- IC2 Energy Storage
   ["ic2_te_semifluid_generator"] = commonBehaviors["ic2_energy"],
   ["ic2_te_electric_furnace"] = commonBehaviors["ic2_energy"],
   ["ic2_te_water_generator"] = commonBehaviors["ic2_energy"],
   ["ic2_te_solar_generator"] = commonBehaviors["ic2_energy"],
   ["ic2_te_wind_generator"] = commonBehaviors["ic2_energy"],
   ["ic2_te_geo_generator"] = commonBehaviors["ic2_energy"],
   ["ic2_te_rt_generator"] = commonBehaviors["ic2_energy"],
   ["ic2_te_generator"] = commonBehaviors["ic2_energy"],
   ["ic2_te_macerator"] = commonBehaviors["ic2_energy"],
   ["ic2_te_condenser"] = commonBehaviors["ic2_energy"],
   ["ic2_te_chargepad_batbox"] = commonBehaviors["ic2_energy"],
   ["ic2_te_batbox"] = commonBehaviors["ic2_energy"],
   ["ic2_te_chargepad_cesu"] = commonBehaviors["ic2_energy"],
   ["ic2_te_cesu"] = commonBehaviors["ic2_energy"],
   ["ic2_te_chargepad_mfsu"] = commonBehaviors["ic2_energy"],
   ["ic2_te_mfsu"] = commonBehaviors["ic2_energy"],
   ["ic2_te_chargepad_mfe"] = commonBehaviors["ic2_energy"],
   ["ic2_te_mfe"] = commonBehaviors["ic2_energy"],
   ["reactor"] = commonBehaviors["ic2_energy"],

}

----------------------------------------
-- Pretty name definitions
----------------------------------------

prettyNames = {

   ["ag_syngas_producer"] = "Syngas Producer",
   ["ag_heat_exchanger"] = "Heat Exchanger",
   ["ag_steam_turbine"] = "Steam Turbine",
   ["ag_gas_turbine"] = "Gas Turbine",

   ["ic2_te_semifluid_generator"] = "Semifluid Generator",
   ["ic2_te_electric_furnace"] = "Electric Furnace",
   ["ic2_te_water_generator"] = "Water Mill",
   ["ic2_te_solar_generator"] = "Solar Panel",
   ["ic2_te_wind_generator"] = "Wind Mill",
   ["ic2_te_geo_generator"] = "Geothermal",
   ["ic2_te_rt_generator"] = "RITEG",
   ["ic2_te_generator"] = "Generator",
   ["ic2_te_macerator"] = "Macerator",
   ["ic2_te_condenser"] = "Condenser",
   ["ic2_te_chargepad_batbox"] = "BatBox (Charge Pad)",
   ["ic2_te_batbox"] = "BatBox",
   ["ic2_te_chargepad_cesu"] = "CESU (Charge Pad)",
   ["ic2_te_cesu"] = "CESU",
   ["ic2_te_chargepad_mfsu"] = "MFSU (Charge Pad)",
   ["ic2_te_mfsu"] = "MFSU",
   ["ic2_te_chargepad_mfe"] = "MFE (Charge Pad)",
   ["ic2_te_mfe"] = "MFE",
   ["reactor"] = "Reactor",

}

----------------------------------------
-- Private helper functions
----------------------------------------

-- pull
-- Returns the desired property from the behavior of the specified component
local function pull( address, name, property )

   local behavior = {}

   -- Address-specific behaviors carry priority over generic name-based behaviors
   if address ~= nil and componentBehaviors[ address ] ~= nil then
	  behavior = componentBehaviors[ address ]
   elseif name ~= nil and componentBehaviors[ name ] ~= nil then
	  behavior = componentBehaviors[ name ]
   else
	  return nil
   end

   -- Return the desired property, if specified
   if property ~= nil then
	  return behavior[ property ]
   else
	  return behavior
   end

end

----------------------------------------
-- Function definitions
----------------------------------------

-- registerComponent
-- Adds component to the list of registered components
function componentMgr:registerComponent( address, name )
   registeredComponents[ address ] = name
end

-- getComponents
-- Returns the registered component list
function componentMgr:getComponents()
   return registeredComponents
end

-- clearComponents
-- Clears the registered component list
function componentMgr:clearComponents()
   registeredComponents = {}
end

-- numComponents
-- Returns number of registered components
function componentMgr:numComponents()
   local count = 0
   for k,v in pairs( registeredComponents ) do count = count + 1 end
   return count
end


-- getBehaviorOf
-- Returns component behavior specific to the provided address or name
function componentMgr:getBehaviorOf( address, name )
   return pull( address, name )
end

-- getTagOf
-- Returns tag of the specified component
function componentMgr:getTagOf( address, name )
   return pull( address, name, "tag" )
end

-- getDescriptionOf
-- Returns description of the specified component
function componentMgr:getDescriptionOf( address, name )
   return pull( address, name, "description" )
end

-- getBlockTypeOf
-- Returns block type of the specified component
function componentMgr:getBlockTypeOf( address, name )
   return pull( address, name, "blockType" )
end

-- getGraphsOf
-- Returns graph data of the specified component
function componentMgr:getGraphsOf( address, name )
   return pull( address, name, "graphs" )
end

-- getCurrentValStrOf
function componentMgr:getCurrentValStrOf( address, name )
   local fn = pull( address, name, "getCurrentValStr" )
   if fn ~= nil then
	  return fn( component.proxy( component.get( address ) ) )
   else
	  return ""
   end
end

-- getMaxValStrOf
function componentMgr:getMaxValStrOf( address, name )
   local fn = pull( address, name, "getMaxValStr" )
   if fn ~= nil then
	  return fn( component.proxy( component.get( address ) ) )
   else
	  return ""
   end
end


-- activate
-- Triggers the onActivate() function of the specified component
function componentMgr:activate( address, name )
   local fn = pull( address, name, "onActivate" )
   if fn ~= nil then
	  fn( component.proxy( component.get( address ) ) )
	  return true
   else
	  return false
   end
end


-- getProxyObjectOf
-- Returns the proxy object of the specified component
function componentMgr:getProxyObjectOf( address, name )
   return component.proxy( component.get( address ) )
end


-- getCurrentValStrFnOf
-- Returns the getCurrentValStr() function of the specified component
function componentMgr:getCurrentValStrFnOf( address, name )
   return pull( address, name, "getCurrentValStr" )
end

-- getMaxValStrFnOf
-- Returns the getMaxValStr() function of the specified component
function componentMgr:getMaxValStrFnOf( address, name )
   return pull( address, name, "getMaxValStr" )
end

-- getOnActivateFnOf
-- Returns the onActivate() function of the specified component
function componentMgr:getOnActivateFnOf( address, name )
   return pull( address, name, "onActivate" )
end


-- setBehaviors
-- Sets a new table of component behaviors
function componentMgr:setBehaviors( behaviors )
   componentBehaviors = behaviors
end

-- addBehaviors
-- Merges a new table of behaviors with the current table
function componentMgr:addBehaviors( behaviors )
   for componentID, behavior in pairs( behaviors ) do
	  componentBehaviors[ componentID ] = behavior
   end
end


-- getPrettyName
-- Returns a readable version of the provided technical component name
function componentMgr:getPrettyName( name )
   return prettyNames[ name ]
end

----------------------------------------
-- Return created module
----------------------------------------

return componentMgr
