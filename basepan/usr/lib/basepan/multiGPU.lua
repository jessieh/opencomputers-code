----------------------------------------
-- multiGPU
-- GPU Manager Module for basepan
-- Programmed by Jessie Hildebrandt
----------------------------------------

----------------------------------------
-- Module requirements
----------------------------------------

local component = require( 'component' )

----------------------------------------
-- Module prototype
----------------------------------------

local multiGPU = {}

----------------------------------------
-- Module variables
----------------------------------------

multiGPU.color = {
   WHITE = 0,
   ORANGE = 1,
   DARKPINK = 2,
   BLUE = 3,
   YELLOW = 4,
   GREEN = 5,
   PINK = 6,
   GREY = 7,
   LIGHTGREY = 8,
   DARKBLUE = 9,
   PURPLE = 10,
   INDIGO = 11,
   BROWN = 12,
   OLIVE = 13,
   RED = 14,
   BLACK = 15
}

multiGPU.errors = {
   ERROR_NO_HARDWARE_REGISTERED = 1,
   ERROR_NO_GPU_OR_SCREEN = 2,
   ERROR_SETTING_RESOLUTION = 3,
   ERROR_SETTING_DEPTH = 4
}

----------------------------------------
-- Local variables
----------------------------------------

local primaryGPU = nil
local primaryScreen = nil

local registeredGPUs = {}
local registeredScreens = {}

----------------------------------------
-- Private helper functions
----------------------------------------

-- clearRegisteredVideoHardware
-- Clears all registered video hardware
local function clearRegisteredVideoHardware()
   primaryGPU = nil
   primaryScreen = nil
   registeredGPUS = {}
   registeredScreens = {}
end

-- registerVideoHardware
-- Detects and registers all GPUs and screens
local function registerVideoHardware()

   -- Register the primary GPU and screen
   primaryGPU = component.getPrimary( "gpu" )
   primaryScreen = component.getPrimary( "screen" )

   -- Add all GPUs and screens to the registration tables
   for address, name in component.list() do

      if name == "gpu" then
         table.insert( registeredGPUs, component.proxy( component.get( address ) ) )
      end

      if name == "screen" then
         table.insert( registeredScreens, component.proxy( component.get( address ) ) )
      end

   end

end

----------------------------------------
-- Function definitions
----------------------------------------

-- initializeGPUs
-- Binds all registered GPUs to a screen
function multiGPU:initializeGPUs()

   clearRegisteredVideoHardware()

   registerVideoHardware()

   -- Check that there is at least one GPU and one screen installed
   if primaryGPU == nil or primaryScreen == nil then
      return multiGPU.errors.ERROR_NO_GPU_OR_SCREEN
   end

   -- Bind the primary GPU to the primary screen
   primaryGPU.bind( primaryScreen.address )

   -- Bind each secondary GPU to a secondary screen
   for tableIndex = 1, #registeredGPUs do

      -- Make sure we don't rebind the primary GPU
      if registeredGPUs[tableIndex].address ~= primaryGPU.address then

         -- If we've run out of screens to bind to, unregister this GPU
         if tableIndex > #registeredScreens then
            registeredGPUs[tableIndex] = nil
         else
            registeredGPUs[tableIndex].bind( registeredScreens[tableIndex].address )
         end

      end

   end

end

-- setForeground
-- Sets the foreground color for all registered GPUs
function multiGPU:setForeground( color, isPaletteIndex )
   for _, gpu in pairs( registeredGPUs ) do
      gpu.setForeground( color, isPaletteIndex )
   end
end

-- setBackground
-- Sets the background color for all registered GPUs
function multiGPU:setBackground( color, isPaletteIndex )
   for _, gpu in pairs( registeredGPUs ) do
      gpu.setBackground( color, isPaletteIndex )
   end
end

-- fill
-- Fills in a section of the screen for all registered GPUs
function multiGPU:fill( x, y, width, height, char )
   for _, gpu in pairs( registeredGPUs ) do
      gpu.fill( x, y, width, height, char )
   end
end

-- fillLine
-- Fills in a line of the screen for all registered GPUs
function multiGPU:fillLine( y )
   for _, gpu in pairs( registeredGPUs ) do
      local width, height = gpu.getResolution()
      gpu.fill( 1, y, width, 1, " " )
   end
end

-- fillColumn
-- Fills in a column of the screen for all registered GPUs
function multiGPU:fillColumn( x )
   for _, gpu in pairs( registeredGPUs ) do
      local width, height = gpu.getResolution()
      gpu.fill( x, 1, 1, height, " " )
   end
end

-- set
-- Writes a string to the screen for all registered GPUs
function multiGPU:set( x, y, value, vertical )
   for _, gpu in pairs( registeredGPUs ) do
      gpu.set( x, y, tostring( value ), vertical )
   end
end

-- clearScreen
-- Clears the screen for all registered GPUs
function multiGPU:clearScreen()
   for _, gpu in pairs( registeredGPUs ) do
      local width, height = gpu.getResolution()
      gpu.fill( 1, 1, width, height, " " )
   end
end

-- setResolution
-- Sets the resolution for all registered GPUs
function multiGPU:setResolution( width, height )

   local errorOccurred = false

   -- Set the resolution for all registered GPUs
   for _, gpu in pairs( registeredGPUs ) do

      -- Check the current resolution
      local currentWidth, currentHeight = gpu.getResolution()

      -- Attempt to set the resolution if needed
      if ( currentWidth ~= width or currentHeight ~= height )
      and ( not gpu.setResolution( width, height ) ) then
         errorOccurred = true
      end

   end

   -- Return the appropriate code if an error occurred
   if errorOccurred then
      return multiGPU.errors.ERROR_SETTING_RESOLUTION
   end

end

-- getResolution
-- Returns the resolution of the primary GPUs
function multiGPU:getResolution()
   return primaryGPU.getResolution()
end

-- setDepth
-- Sets the color depth of all registered GPUs
function multiGPU:setDepth( depth )

   local errorOccurred = false

   -- Set the depth for all registered GPUs
   for _, gpu in pairs( registeredGPUs ) do
      if not gpu.getDepth() == depth
      and not gpu.setDepth( depth ) then
         errorOccurred = true
      end
   end

   -- Return the appropriate code if an error occurred
   if errorOccurred then
      return multiGPU.errors.ERROR_SETTING_DEPTH
   end

end

-- maxDepth
-- Returns the highest common maximum depth supported by all GPUs
function multiGPU:maxDepth()

   local maxDepth = 8

   -- Make sure that we don't erroneously return "8" if there are no GPUs present
   if #registeredGPUs == 0 then
      return 0
   end

   -- Check the max depth of all registered GPUs
   for _, gpu in pairs( registeredGPUs ) do
      maxDepth = math.min( maxDepth, gpu.maxDepth() )
   end

   return maxDepth

end

-- setTouchModeInverted
-- Sets inverted touch mode for all bound screens
function multiGPU:setTouchModeInverted( enabled )
   for _, gpu in pairs( registeredGPUs ) do
      local screenObj = component.proxy( gpu.getScreen() )
      screenObj.setTouchModeInverted( enabled )
   end
end

-- isTouchModeInverted
-- Returns true if touch mode is set to inverted on the primary screen
function multiGPU:isTouchModeInverted()
   return primaryScreen.isTouchModeInverted()
end

----------------------------------------
-- Return created module
----------------------------------------

return multiGPU
