----------------------------------------
-- basepan
-- Component Control Panel for OpenComputers
-- Programmed by Jessie Hildebrandt
----------------------------------------

-- local colorPalette = {
--    16777215, -- White          [0]
--    16763955, -- Orange         [1]
--    13395660, -- Dark Pink      [2]
--    6724095,  -- Sky Blue       [3]
--    16777011, -- Yellow         [4]
--    3394611,  -- Green          [5]
--    16737945, -- Pink           [6]
--    33554433, -- Grey           [7]
--    13421772, -- Light Grey     [8]
--    3368601,  -- Dark Sky Blue  [9]
--    10040268, -- Purple         [10]
--    3355545,  -- Indigo         [11]
--    6697728,  -- Brown          [12]
--    3368448,  -- Olive Green    [13]
--    16724787, -- Red            [14]
--    0         -- Black          [15]
-- }

----------------------------------------
-- Module requirements
----------------------------------------

local computer = require( 'computer' )
local component = require( 'component' )
local event = require( 'event' )
local term = require( 'term' )
local colors = require( 'colors' )

local componentMgr = require( 'basepan/componentMgr' )
local multiGPU = require( 'basepan/multiGPU' )

----------------------------------------
-- Constants
----------------------------------------

local COLOR = {
   white = 0,
   orange = 1,
   darkPink = 2,
   blue = 3,
   yellow = 4,
   green = 5,
   pink = 6,
   grey = 7,
   lightGrey = 8,
   darkBlue = 9,
   purple = 10,
   indigo = 11,
   brown = 12,
   olive = 13,
   red = 14,
   black = 15
}

local CONFIG_FILE_PATH = '/etc/basepan.cfg'

----------------------------------------
-- Variables
----------------------------------------

local cfg = {}
local componentBlocks = {}

----------------------------------------
-- Function definitions
----------------------------------------

-- initializeVideo
-- Sets up the video hardware
local function initializeVideo()

   -- Clear the primary screen
   term.clear()

   -- Initialize the video hardware
   if multiGPU:initializeGPUs() == multiGPU.errors.ERROR_NO_GPU_OR_SCREEN then
	  print( "A GPU and screen are required to run basepan." )
	  os.exit( -1 )
   end

   -- Clear all secondary screens
   multiGPU:clearScreen()

   -- Check for the appropriate color depth
   if multiGPU:maxDepth() < 4 then
      print( "You have incompatible (Tier 1) screens and/or GPUs connected to this computer." )
      print( "Please ensure that all of your currently-connected video hardware is compatible with basepan." )
      os.exit( -1 )
   end

   -- Attempt to set the appropriate color depth
   if multiGPU:setDepth( 4 ) == multiGPU.errors.ERROR_SETTING_DEPTH then
      print( "An error occurred while setting the color depth." )
      os.exit( -1 )
   end

   -- Attempt to set the screen resolution
   if multiGPU:setResolution( 80, 25 ) == multiGPU.errors.ERROR_SETTING_RESOLUTION then
      print( "An error occurred while setting the screen resolution." )
      os.exit( -1 )
   end

   -- Turn on inverted touch mode for easier operation
   multiGPU:setTouchModeInverted( true )

end

-- refreshComponentList
-- Updates the list of compatible components
local function refreshComponentList()

   print( "Checking components..." )

   componentMgr:clearComponents()
   componentBlocks = {}

   for address, name in component.list() do
      print( address, name )
	  if componentMgr:getBehaviorOf( address, name ) then
		 print( 'Component ' .. componentMgr:getPrettyName( name ) .. ' has a configured behavior. Registered!' )
		 componentMgr:registerComponent( address, name )
	  end
   end

end

-- drawBlock
-- Draws a component block
local function drawBlock( address, name, blockX, blockY, blockSize )

   local blockType = componentMgr:getBlockTypeOf( address, name )
   local graphs = componentMgr:getGraphsOf( address, name )
   local proxyObject = componentMgr:getProxyObjectOf( address, name )

   if blockType == "color" then

      ----------
      -- "Color"-type graphs
      ----------

      for graphID = 1, #graphs do

         -- Determine graph dimensions
         local graphWidth = ( blockSize.w / #graphs )
         local graphX = blockX + ( graphWidth * ( graphID - 1 ) )
         local graphValue = graphs[graphID].getCurrentVal( proxyObject ) / graphs[graphID].getMaxVal( proxyObject )

         -- Determine which colors to use
         local numColors = #graphs[graphID].colors
         local colorID = math.max( math.min( math.floor( numColors * graphValue + 0.5 ), numColors ), 1 )
         multiGPU:setForeground( graphs[graphID].colors[colorID].fgColor, true )
         multiGPU:setBackground( graphs[graphID].colors[colorID].bgColor, true )

         -- Fill in the background only if necessary (slow operation)
         if componentBlocks[address].currentColor == nil or componentBlocks[address].currentColor ~= colorID then
            multiGPU:fill( graphX, blockY, graphWidth, blockSize.h, " " )
            componentBlocks[address].currentColor = colorID
         end

      end

   else

      ----------
      -- All other graphs or typeless graphs
      ----------

      -- Just set the appropriate colors
	  multiGPU:setForeground( graphs[1].fgColor, true )
	  multiGPU:setBackground( graphs[1].bgColor, true )

      -- Fill in the background only if necessary (slow operation)
      if componentBlocks[address].backgroundDrawn == nil then
         multiGPU:fill( blockX, blockY, blockSize.w, blockSize.h, " " )
         componentBlocks[address].backgroundDrawn = true
      end

   end

end

-- drawScreen
-- Updates the display
local function drawScreen()

   -- Determine size of blocks to draw
   local blockSize = { w = 24, h = 20 }
   local blockGap = { x = 3, y = 2 };
   if  componentMgr:numComponents() > 3 then
	  blockSize = { w = 24, h = 6 }
   elseif componentMgr:numComponents() > 9 then
	  blockSize = { w = 12, h = 6 }
	  blockGap = { x = 1, y = 1 };
   end

   -- Draw blocks on screen
   local blockX = 2
   local blockY = 1
   for address, name in pairs( componentMgr.getComponents() ) do

      -- Register this block in the data table if it isn't already there
      if componentBlocks[address] == nil then
         componentBlocks[address] = {}
      end

	  -- Fill out block
      drawBlock( address, name, blockX, blockY, blockSize )

	  -- Write component name
      multiGPU:set( blockX, blockY, componentMgr:getPrettyName( name ) )

	  -- Write current value
      multiGPU:set( blockX, blockY + 2, componentMgr:getCurrentValStrOf( address, name ) )
      multiGPU:set( blockX, blockY + 3, componentMgr:getMaxValStrOf( address, name ) )

	  -- Left-to-right layout increment
	  blockX = blockX + blockSize.w + blockGap.x
	  if ( blockX >= 80 ) then
	  	 blockX = 2
	  	 blockY = blockY + blockSize.h + blockGap.y
	  end

	  -- Top-to-bottom layout increment
	  -- blockY = blockY + blockSize.h + blockGap.y
	  -- if ( blockY >= 23 ) then
	  -- 	 blockY = 1
	  -- 	 blockX = blockX + blockSize.w + blockGap.x
	  -- end

   end

   -- Draw status bar line #1
   local dateString = os.date( " [%H:%M]" )
   local componentsString = tostring( componentMgr.numComponents() )
   local powerLevelString = math.floor( computer.energy() ) .. " / " .. math.floor( computer.maxEnergy() )
   multiGPU:setBackground( COLOR.blue, true )
   multiGPU:fillLine( 24 ) -- Clear out the line
   multiGPU:setForeground( COLOR.black, true ) -- Draw all black text
   multiGPU:set( 1 + #dateString, 24, " >> Components : " )
   if ( cfg.displayMachinePower ) then
      multiGPU:set( 18 + #dateString + #componentsString, 24, " >> Computer Power : " )
   end
   multiGPU:setForeground( COLOR.grey, true ) -- Draw all grey text
   multiGPU:set( 1, 24, dateString )
   multiGPU:set( 18 + #dateString, 24, componentsString )
   if ( cfg.displayMachinePower ) then
      multiGPU:set( 39 + #dateString + #componentsString, 24, powerLevelString )
   end

   -- Draw status bar line #2
   local panelNameString = " " .. cfg.panelName
   multiGPU:setForeground( COLOR.lightGrey, true )
   multiGPU:setBackground( COLOR.darkBlue, true )
   multiGPU:fillLine( 25 )
   multiGPU:set( 1, 25, panelNameString )

end

-- handleTouch
-- Handles touch events and triggers block actions
local function handleTouch( touchX, touchY )

   -- Determine size of blocks
   local blockSize = { w = 24, h = 20 }
   local blockGap = { x = 3, y = 2 };
   if  componentMgr:numComponents() > 3 then
	  blockSize = { w = 24, h = 6 }
   elseif componentMgr:numComponents() > 9 then
	  blockSize = { w = 12, h = 6 }
	  blockGap = { x = 1, y = 1 };
   end

   -- Find block that was booped or something
   local blockX = 2
   local blockY = 1
   for address, name in pairs( componentMgr:getComponents() ) do

	  -- Check for touch
	  if touchX >= blockX and touchX < blockX + blockSize.w
	  and touchY >= blockY and touchY < blockY + blockSize.h then
		 if componentMgr:activate( address, name ) and cfg.beepOnTouch then
			computer.beep( 400, 0.01 )
         end
         break
	  end

	  -- Left-to-right layout increment
	  blockX = blockX + blockSize.w + blockGap.x
	  if ( blockX >= 80 ) then
	  	 blockX = 2
	  	 blockY = blockY + blockSize.h + blockGap.y
	  end

   end

end

----------------------------------------
-- main
----------------------------------------

local function main()

   -- Load configuration file
   do
      print( "Loading configuration file..." )
      local cfgFile = loadfile( CONFIG_FILE_PATH, "t", cfg )
	  cfgFile()
   end

   -- Refresh component list
   refreshComponentList()
   os.sleep(1)

   -- Initialize the video configuration
   initializeVideo()

   -- Boo-beep
   computer.beep( 523 )
   computer.beep( 1046 )

   -- Main loop
   while true do

	  -- Pull in an event
	  local eventID, _,param1, param2 = event.pullMultiple( 1,
															 "touch",
															 "component_added", "component_removed",
															 "interrupted" )

	  -- Handle the event
	  if eventID == "touch" then
		 handleTouch( param1, param2 )
	  elseif eventID == "component_added" or eventID == "component_removed" then
		 refreshComponentList()
		 initializeVideo() -- Just in case the video configuration was changed
		 if componentMgr:numComponents() > 18 then
			print( 'Too many registered components!' )
			break
		 end
	  elseif eventID == "interrupted" then
		 break
	  end

	  -- Draw the screen
	  drawScreen()

   end

   -- Clear the screen and revert the touch mode settings
   multiGPU:setTouchModeInverted( false )
   multiGPU:clearScreen()
   term.clear()

   -- Reset colors to defaults
   multiGPU:setForeground( COLOR.white, true )
   multiGPU:setBackground( COLOR.black, true )

   -- Bye!
   print( 'Program terminated.' )
   print( 'Thank you for using basepan.' )

end

----------------------------------------
-- Execution
----------------------------------------

main()
