local component = require("component")
local shell = require("shell")
local event = require("event")
local computer = require("computer")
local modem = nil

-- Try to initialize a wireless modem.
if component.list("modem")() then modem = component.modem end

-- Make sure we're already connected to a drone.
local status, port = shell.execute("dronecom status -q")
if not port then
  print("Not connected to any drones!")
  print("Use 'dronecom connect' to connect to an unpaired drone.")
  os.exit(-1)
end

-- Make sure that the connected drone has a leash available.
modem.broadcast(port, "return isComponentAvailable('leash')")
local leashAvailable = select(6, event.pull(3, "modem_message"))
if not leashAvailable then
  print("The connected drone has no leash available.")
  print("This program requires a leash to run")
end

-- Define a table of mobs to defend the motion sensor from.
local mobNames = {
  "Cow",
  "Pig",
  "Sheep"
}

-- Define a table of non-targetable mobs to warn the user about.
local threatNames = {
  "Skeleton",
  "Zombie",
  "Slime",
  "Blaze",
  "Enderman"
}

-- Define a table of non-targetable mobs to REALLY warn the user about.
local highThreatNames = {
  "Creeper",
  "Ghast"
}

-- Define a table of key locations for the drone to fly to.
local locations = {
  charger = {
    x = 1.49,
    y = 5.25,
    z = -46.49
  },
  entryHole = {
    x = 7.00,
    y = 5.35,
    z = -46.49
  },
  motionSensor = {
    x = 4.50,
    y = 7.35,
    z = -48.50
  }
}

-- Let the user know that we're starting, as well as how to exit.
print("Starting defense routine!")
print("Press 'q' to exit.")

-- Change the drone's light color.
modem.broadcast(port, "drone.setLightColor(lightColors.red)")
event.pull(5, "modem_message")

-- Start the main loop.
local keypressed = 0
while keypressed ~= 16 do

  -- Update the keypressed value and check for any movement.
  keypressed = select(4, event.pull(1, "key_down"))
  local eventName, address, entityX, entityY, entityZ, entityName = event.pull(1, "motion")
  
  -- Check to see if we spotted movement.
  if eventName then
  
    -- Alert the player to the movement and see what moved.
    print("Movement!")
    print(threatLevel)
    local threatlevel = "none"
    for name = 1, #mobNames do
      if entityName == mobNames[name] then threatLevel = "targetable" end
    end
    if threatLevel == "none" then
      for name = 1, #threatNames do
        if entityName == threatNames[name] then threatLevel = "threat" end
      end
    end
    if threatLevel == "none" then
      for name = 1, #highThreatNames do
        if entityName == highThreatNames[name] then threatLevel = "highThreat" end
      end
    end
  
    -- If it's targetable, execute it!
    if threatLevel == "targetable" then
    
      -- Alert the user to the target.
      computer.beep(800, 0.1)
      computer.beep(800, 0.1)
      print("Found a " .. entityName .. "!")
      
      -- Calculate the entity's position.
      local entityX = locations.motionSensor.x + entityX
      local entityY = locations.motionSensor.y + entityY
      local entityZ = locations.motionSensor.z + entityZ
      
      -- Change the drone's light color. He's angry.
      modem.broadcast(port, "drone.setLightColor(lightColors.red)")
      event.pull(5, "modem_message")
      
      -- Undock from the charging port and fly to the exit/entry hole.
      print("Undocking.")
      modem.broadcast(port, "flyTo(" .. locations.entryHole.x .. ", " .. locations.entryHole.y .. ", " .. locations.entryHole.z .. ")")
      event.pull(5, "modem_message")
      os.sleep(0.25)
      
      -- Fly over to the target in question.
      print("Flying to target.")
      modem.broadcast(port, "flyTo(" .. entityX .. ", " .. entityY .. ", " .. entityZ .. ")")
      event.pull(5, "modem_message")
      os.sleep(0.25)
      
      -- Leash the threat to the drone.
      print("Leashing...")
      modem.broadcast(port, "return leashNearest()")
      local leashed = select(6, event.pull(5, "modem_message"))
      os.sleep(0.10)
      
      if leashed then
      
      -- Fly up into space.
      print("Flying to space...")
      modem.broadcast(port, "move(" .. locations.entryHole.x .. ", 60, " .. locations.entryHole.z .. ")")
      event.pull(5, "modem_message")
      os.sleep(10)
      
      -- Drop the threat to their doom.
      print("Executing...")
      modem.broadcast(port, "leash.unleash()")
      event.pull(5, "modem_message")
      os.sleep(0.10)
      
      else
      
        print("Failed to leash.")
      
      end
      
      -- Fly back to the exit/entry home.
      print("Returning home...")
      modem.broadcast(port, "flyTo(" .. locations.entryHole.x .. ", " .. locations.entryHole.y .. ", " .. locations.entryHole.z .. ")")
      event.pull(5, "modem_message")
      os.sleep(10)
      
      -- Settle back down on the charging port.
      print("Docking...")
      modem.broadcast(port, "flyTo(" .. locations.charger.x .. ", " .. locations.charger.y .. ", " ..locations.charger.z .. ")")
      event.pull(5, "modem_message")
      os.sleep(0.50)
      
      -- The drone is happy now.
      print("Docked.")
      modem.broadcast(port, "drone.setLightColor(lightColors.blue)")
      event.pull(5, "modem_message")
      os.sleep(0.10)
      
    elseif threatLevel == "threat" then
    
      -- Alert the player to the threat.
      computer.beep(900, 0.2)
      computer.beep(900, 0.2)
      computer.beep(900, 0.2)
      print("Found a " .. entityName .. "!")
      print("Non-targetable. Be careful, it's dangerous!")
      
    elseif threatLevel == "highThreat" then
    
      -- Alert the player to the threat.
      computer.beep(1000, 0.35)
      computer.beep(1000, 0.35)
      computer.beep(1000, 0.35)
      print("RED ALERT! Found a " .. entityName .. "!")
      print("Non-targetable. Be careful, it's VERY dangerous!")
      
    else print("Oh, it's just (a) " .. entityName .. ".") end
    
  end
  
end

-- Change the drone's light color back to normal.
modem.broadcast(port, "drone.setLightColor(lightColors.green)")